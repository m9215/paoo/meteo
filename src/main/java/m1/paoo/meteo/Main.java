package m1.paoo.meteo;

import org.apache.commons.cli.*;

import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        int id = 0;
        String name = null;
        String countryCode = "FR";
        Options options = new Options();
        Option n = new Option("n", "name", true, "nom de la ville");
        n.setRequired(true);
        Option c = new Option("c", "code", true, "code du pays (défault=FR)");
        c.setRequired(false);
        options.addOption(n).addOption(c);

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine line = parser.parse(options, args);
            if (line.hasOption("c"))
                countryCode = line.getOptionValue("c");
            name = line.getOptionValue("n");
        } catch (ParseException e) {
            LOG.severe("Erreur dans la ligne de commande");
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("serialize", options);
            System.exit(1);
        }

        LOG.info("Démarrage du traitement");
        CityClient cityClient = new CityClient(name, countryCode);
        try {
            System.out.printf("%s%n", cityClient.getCityByName());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        LOG.info("Terminé");
    }

}
