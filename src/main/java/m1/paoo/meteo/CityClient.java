package m1.paoo.meteo;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

public class CityClient {

    private String name;
    private String countryCode;
    public static final String WS_URL = "https://geocoding-api.open-meteo.com/v1/search";
    private static final Logger LOG = Logger.getLogger(CityClient.class.getName());

    public CityClient(String name, String countryCode) {
        this.name = name;
        this.countryCode = countryCode;
    }

    public CityClient(String name) {
        this.name = name;
    }

    public String getCityByName() throws UnsupportedEncodingException {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(WS_URL);
        target = target.queryParam("name", URLEncoder.encode(getName(), StandardCharsets.UTF_8));
        target = target.queryParam("country", URLEncoder.encode(getCountryCode(), StandardCharsets.UTF_8));
        Invocation.Builder builder = target.request(MediaType.APPLICATION_JSON);
        LOG.info(target.getUri().getQuery());
        Response response = builder.get();
        return response.readEntity(String.class);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
